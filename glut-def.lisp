;; -*- mode: Lisp; Syntax: Common-Lisp; Package: cl-opengl; -*-
;;;
;;; Copyright � 1995,2003 by Kenneth William Tilton.
;;;
;;; Permission is hereby granted, free of charge, to any person obtaining a copy 
;;; of this software and associated documentation files (the "Software"), to deal 
;;; in the Software without restriction, including without limitation the rights 
;;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
;;; copies of the Software, and to permit persons to whom the Software is furnished 
;;; to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in 
;;; all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
;;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
;;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
;;; IN THE SOFTWARE.

(in-package :cl-opengl)

#| display mode bit masks. |#
(dfc glut_rgb		0)
(dfc glut_rgba		glut_rgb)
(dfc glut_index		1)
(dfc glut_single		0)
(dfc glut_double		2)
(dfc glut_accum		4)
(dfc glut_alpha		8)
(dfc glut_depth		16)
(dfc glut_stencil		32)
(dfc glut_multisample	128)
(dfc glut_stereo		256)
(dfc glut_luminance	512)


#| mouse buttons. |#
(dfc glut_left_button		0)
(dfc glut_middle_button		1)
(dfc glut_right_button		2)
(dfc glut_mouse_wheel_click   3)
(dfc glut_mouse_wheel_back    4)
(dfc glut_mouse_wheel_fwd     5)

#| mouse button  state. |#
(dfc glut_down			0)
(dfc glut_up			1)


#| function keys |#
(dfc glut_key_f1			1)
(dfc glut_key_f2			2)
(dfc glut_key_f3			3)
(dfc glut_key_f4			4)
(dfc glut_key_f5			5)
(dfc glut_key_f6			6)
(dfc glut_key_f7			7)
(dfc glut_key_f8			8)
(dfc glut_key_f9			9)
(dfc glut_key_f10			10)
(dfc glut_key_f11			11)
(dfc glut_key_f12			12)
#| directional keys |#
(dfc glut_key_left		100)
(dfc glut_key_up			101)
(dfc glut_key_right		102)
(dfc glut_key_down		103)
(dfc glut_key_page_up		104)
(dfc glut_key_page_down		105)
(dfc glut_key_home		106)
(dfc glut_key_end			107)
(dfc glut_key_insert		108)


#| entry/exit  state. |#
(dfc glut_left			0)
(dfc glut_entered			1)


#| menu usage  state. |#
(dfc glut_menu_not_in_use	0)
(dfc glut_menu_in_use		1)


#| visibility  state. |#
(dfc glut_not_visible		0)
(dfc glut_visible			1)

#| window status  state. |#
(dfc glut_hidden			0)
(dfc glut_fully_retained	1)
(dfc glut_partially_retained	2)
(dfc glut_fully_covered		3)

#| color index component selection values. |#
(dfc glut_red			0)
(dfc glut_green			1)
(dfc glut_blue			2)

#| stroke font constants (use these in glut program). |#
(dfc glut_stroke_roman		0)
(dfc glut_stroke_mono_roman	1)

#| bitmap font constants (use these in glut program). |#
(dfc glut_bitmap_9_by_15		2)
(dfc glut_bitmap_8_by_13		3)
(dfc glut_bitmap_times_roman_10	4)
(dfc glut_bitmap_times_roman_24	5)

(dfc glut_bitmap_helvetica_10	6)
(dfc glut_bitmap_helvetica_12	7)
(dfc glut_bitmap_helvetica_18	8)


#| glutget parameters. |#
(dfc glut_window_x			 100)
(dfc glut_window_y			 101)
(dfc glut_window_width		 102)
(dfc glut_window_height		 103)
(dfc glut_window_buffer_size		 104)
(dfc glut_window_stencil_size	 105)
(dfc glut_window_depth_size		 106)
(dfc glut_window_red_size		 107)
(dfc glut_window_green_size		 108)
(dfc glut_window_blue_size		 109)
(dfc glut_window_alpha_size		 110)
(dfc glut_window_accum_red_size	 111)
(dfc glut_window_accum_green_size	 112)
(dfc glut_window_accum_blue_size	 113)
(dfc glut_window_accum_alpha_size	 114)
(dfc glut_window_doublebuffer	 115)
(dfc glut_window_rgba		 116)
(dfc glut_window_parent		 117)
(dfc glut_window_num_children	 118)
(dfc glut_window_colormap_size	 119)

(dfc glut_window_num_samples		 120)
(dfc glut_window_stereo		 121)
(dfc glut_window_cursor		 122)

(dfc glut_screen_width		 200)
(dfc glut_screen_height		 201)
(dfc glut_screen_width_mm		 202)
(dfc glut_screen_height_mm		 203)
(dfc glut_menu_num_items		 300)
(dfc glut_display_mode_possible	 400)
(dfc glut_init_window_x		 500)
(dfc glut_init_window_y		 501)
(dfc glut_init_window_width		 502)
(dfc glut_init_window_height		 503)
(dfc glut_init_display_mode		 504)

(dfc glut_elapsed_time		 700)
(dfc glut_window_format_id		 123)
(dfc glut_init_state #x007c)


#| glutdeviceget parameters. |#
(dfc glut_has_keyboard		 600)
(dfc glut_has_mouse			 601)
(dfc glut_has_spaceball		 602)
(dfc glut_has_dial_and_button_box	 603)
(dfc glut_has_tablet			 604)
(dfc glut_num_mouse_buttons		 605)
(dfc glut_num_spaceball_buttons	 606)
(dfc glut_num_button_box_buttons	 607)
(dfc glut_num_dials			 608)
(dfc glut_num_tablet_buttons		 609)

(dfc glut_device_ignore_key_repeat    610)
(dfc glut_device_key_repeat           611)
(dfc glut_has_joystick		 612)
(dfc glut_owns_joystick		 613)
(dfc glut_joystick_buttons		 614)
(dfc glut_joystick_axes		 615)
(dfc glut_joystick_poll_rate		 616)


#| glutlayerget parameters. |#
(dfc glut_overlay_possible            800)
(dfc glut_layer_in_use		 801)
(dfc glut_has_overlay		 802)
(dfc glut_transparent_index		 803)
(dfc glut_normal_damaged		 804)
(dfc glut_overlay_damaged		 805)


#| glutvideoresizeget parameters. |#
(dfc glut_video_resize_possible	 900)
(dfc glut_video_resize_in_use	 901)
(dfc glut_video_resize_x_delta	 902)
(dfc glut_video_resize_y_delta	 903)
(dfc glut_video_resize_width_delta	 904)
(dfc glut_video_resize_height_delta	 905)
(dfc glut_video_resize_x		 906)
(dfc glut_video_resize_y		 907)
(dfc glut_video_resize_width		 908)
(dfc glut_video_resize_height	 909)


#| glutuselayer parameters. |#
(dfc glut_normal			 0)
(dfc glut_overlay			 1)


#| glutgetmodifiers return mask. |#
(dfc glut_active_shift               1)
(dfc glut_active_ctrl                2)
(dfc glut_active_alt                 4)

#| glutsetcursor parameters. |#
#| basic arrows. |#
(dfc glut_cursor_right_arrow		0)
(dfc glut_cursor_left_arrow		1)
#| symbolic cursor shapes. |#
(dfc glut_cursor_info		2)
(dfc glut_cursor_destroy	3)
(dfc glut_cursor_help		4)
(dfc glut_cursor_cycle		5)
(dfc glut_cursor_spray		6)
(dfc glut_cursor_wait		7)
(dfc glut_cursor_text		8)
(dfc glut_cursor_crosshair		9)
#| directional cursors. |#
(dfc glut_cursor_up_down		10)
(dfc glut_cursor_left_right		11)
#| sizing cursors. |#
(dfc glut_cursor_top_side		12)
(dfc glut_cursor_bottom_side		13)
(dfc glut_cursor_left_side		14)
(dfc glut_cursor_right_side		15)
(dfc glut_cursor_top_left_corner	16)
(dfc glut_cursor_top_right_corner	17)
(dfc glut_cursor_bottom_right_corner	18)
(dfc glut_cursor_bottom_left_corner	19)
#| inherit from parent window. |#
(dfc glut_cursor_inherit		100)
#| blank cursor. |#
(dfc glut_cursor_none		101)

#| fullscreen crosshair |#
(dfc glut_cursor_full_crosshair	102)

#| glut device control sub-api. |#
#| glutsetkeyrepeat modes. |#
(dfc glut_key_repeat_off		0)
(dfc glut_key_repeat_on		1)
(dfc glut_key_repeat_default		2)

#| joystick button masks. |#
(dfc glut_joystick_button_a		1)
(dfc glut_joystick_button_b		2)
(dfc glut_joystick_button_c		4)
(dfc glut_joystick_button_d		8)

#| glut game mode sub-api. |#
#| glutgamemodeget. |#
(dfc glut_game_mode_active            0)
(dfc glut_game_mode_possible          1)
(dfc glut_game_mode_width             2)
(dfc glut_game_mode_height            3)
(dfc glut_game_mode_pixel_depth       4)
(dfc glut_game_mode_refresh_rate      5)
(dfc glut_game_mode_display_changed   6)
