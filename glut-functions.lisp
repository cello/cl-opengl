;; -*- mode: Lisp; Syntax: Common-Lisp; Package: cl-opengl; -*-
;;;
;;; Copyright � 1995,2003 by Kenneth William Tilton.
;;;
;;; Permission is hereby granted, free of charge, to any person obtaining a copy 
;;; of this software and associated documentation files (the "Software"), to deal 
;;; in the Software without restriction, including without limitation the rights 
;;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
;;; copies of the Software, and to permit persons to whom the Software is furnished 
;;; to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in 
;;; all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
;;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
;;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
;;; IN THE SOFTWARE.

(in-package #:cl-opengl)

#| glut window sub-api. |#




#| font weights |#
(dfc fw_dontcare         0)
(dfc fw_thin             100)
(dfc fw_extralight       200)
(dfc fw_light            300)
(dfc fw_normal           400)
(dfc fw_medium           500)
(dfc fw_semibold         600)
(dfc fw_bold             700)
(dfc fw_extrabold        800)
(dfc fw_heavy            900)

(dfc fw_ultralight       fw_extralight)
(dfc fw_regular          fw_normal)
(dfc fw_demibold         fw_semibold)
(dfc fw_ultrabold        fw_extrabold)
(dfc fw_black            fw_heavy)


(dfc wgl_font_lines      0)
(dfc wgl_font_polygons   1)

(dfc glut_action_exit                         0)
(dfc glut_action_glutmainloop_returns         1)
(dfc glut_action_continue_execution           2)
(dfc glut_action_on_window_close        #x01f9)

(defun-ffx :void "glut" "glutSetOption" (glenum e-what :int value))
;;;(defun-ffx :void "glut" "glutWFill" (:float r :float g :float b :float alpha))
;;;(defun-ffx :void "glut" "glutWFill2" (:float r :float g :float b :float alpha))
;;;(defun-ffx :void "glut" "glutWClearColor" (:float r :float g :float b :float alpha))
;;;(defun-ffx :void "glut" "glutWClear" ())

(defun-ffx :int "glut" "glutCreateWindow" (:cstring title))
(defun-ffx :int "glut" "glutCreateSubWindow" (:int win :int x :int y :int width :int height))
(defun-ffx :void "glut" "glutDestroyWindow" (:int win))
;;;(defun-ffx :void "glut" "fgDeinitialize" ())


(ff-defun-callable  :cdecl :void mgwclose ()
  (print "closing callback entered"))

(eval-when (compile load eval)
  (export '(mgwclose freeg glut-bitmap-string glut-stroke-string)))

(defun freeg () t)

(defun-ffx :void "glut" "glutPostRedisplay" ())
(defun-ffx :void "glut" "glutPostWindowRedisplay" (:int win))
(defun-ffx :void "glut" "glutSwapBuffers" ())
(defun-ffx :int "glut" "glutGetWindow" ())
;;;(defun-ffx :int "glut" "glutDestroyPending" ())
(defun-ffx :void "glut" "glutSetWindow" (:int win))
(defun-ffx :void "glut" "glutSetWindowTitle" (:cstring title))
(defun-ffx :void "glut" "glutSetIconTitle" (:cstring title))
(defun-ffx :void "glut" "glutPositionWindow" (:int x :int y))
(defun-ffx :void "glut" "glutReshapeWindow" (:int width :int height))
(defun-ffx :void "glut" "glutPopWindow" ())
(defun-ffx :void "glut" "glutPushWindow" ())
(defun-ffx :void "glut" "glutIconifyWindow" ())
(defun-ffx :void "glut" "glutShowWindow" ())
(defun-ffx :void "glut" "glutHideWindow" ())
(defun-ffx :void "glut" "glutFullScreen" ())
(defun-ffx :void "glut" "glutSetCursor" (:int cursor))
(defun-ffx :void "glut" "glutWarpPointer" (:int x :int y))



#-cormanlisp
(defun-ffx :void "glut" "glutInit" (:int *argc :void *argv))

#+original-cormanlisp
(ff-def-call ("glut" glut-init "glutInit")
                      ((argc (* :int))
                       (argv (* :void))))

#+cormanlisp
(ff-def-call ("glut" glut-init "glutInit")
                      ((argc (:int *))
                       (argv (:void *))))

(defun-ffx :void "glut" "glutInitDisplayMode" (gluint mode)) ;; .h said "unsigned int"

(defun-ffx :void "glut" "glutInitWindowPosition" (glint x glint y)) ;; .h said ints
(defun-ffx :void "glut" "glutInitWindowSize" (glint width glint height)) ;; .h said ints
(defun-ffx :void "glut" "glutInitDisplayString" (:cstring string))
(defun-ffx :void "glut" "glutLeaveMainLoop" ())
(defun-ffx :void "glut" "glutMainLoop" ())
;;;(defun-ffx :void "glut" "glutCheckLoop" ())
(defun-ffx :void "glut" "glutMainLoopEvent" ())


#| glut window callback sub-api. |#

(defun-ffx :void "glut" "glutCloseFunc" (:void *lpfn)) ;; (*func)(void)
(defun-ffx :void "glut" "glutWMCloseFunc" (:void *lpfn)) ;; (*func)(void) ;; same
(defun-ffx :void "glut" "glutDisplayFunc" (:void *lpfn)) ;; (*func)(void)
(defun-ffx :void "glut" "glutReshapeFunc" (:void *lpfn)) ;; (*func)(int width (), int height));
(defun-ffx :void "glut" "glutKeyboardFunc" (:void *lpfn)) ;; (*func)(unsigned char key, int x, int y));
(defun-ffx :void "glut" "glutMouseFunc" (:void *lpfn)) ;; (*func)(int button, int state, int x, int y));
(defun-ffx :void "glut" "glutMotionFunc" (:void *lpfn)) ;; (*func)(int x, int y));
(defun-ffx :void "glut" "glutPassiveMotionFunc" (:void *lpfn)) ;; (*func)(int x, int y));
(defun-ffx :void "glut" "glutEntryFunc" (:void *lpfn)) ;; (*func)(int state));
(defun-ffx :void "glut" "glutVisibilityFunc" (:void *lpfn)) ;; (*func)(int state));
(defun-ffx :void "glut" "glutIdleFunc" (:void *lpfn)) ;; (*func)(void));
(defun-ffx :void "glut" "glutTimerFunc" (gluint millis :long lpfn)) ;; (int value), int value);
(defun-ffx :void "glut" "glutMenuStateFunc" (:void *lpfn)) ;; (*func)(int state));

(defun-ffx :void "glut" "glutSpecialFunc" (:void *lpfn)) ;; (*func)(int key, int x, int y));
(defun-ffx :void "glut" "glutSpaceballMotionFunc" (:void *lpfn)) ;; (*func)(int x, int y, int z));
(defun-ffx :void "glut" "glutSpaceballRotateFunc" (:void *lpfn)) ;; (*func)(int x, int y, int z));
(defun-ffx :void "glut" "glutSpaceballButtonFunc" (:void *lpfn)) ;; (*func)(int button, int state));
(defun-ffx :void "glut" "glutButtonBoxFunc" (:void *lpfn)) ;; (*func)(int button, int state));
(defun-ffx :void "glut" "glutDialsFunc" (:void *lpfn)) ;; (*func)(int dial, int value));
(defun-ffx :void "glut" "glutTabletMotionFunc" (:void *lpfn)) ;; (*func)(int x, int y));
(defun-ffx :void "glut" "glutTabletButtonFunc" (:void *lpfn)) ;; (*func)(int button, int state, int x, int y));

(defun-ffx :void "glut" "glutMenuStatusFunc" (:void *lpfn)) ;; (*func)(int status, int x, int y));
(defun-ffx :void "glut" "glutOverlayDisplayFunc" (:void *lpfn)) ;; (*func)(void));

(defun-ffx :void "glut" "glutWindowStatusFunc" (:void *lpfn)) ;; (*func)(int state));

(defun-ffx :void "glut" "glutKeyboardUpFunc" (:void *lpfn)) ;; (*func)(unsigned char key, int x, int y));
(defun-ffx :void "glut" "glutSpecialUpFunc" (:void *lpfn)) ;; (*func)(int key, int x, int y));
(defun-ffx :void "glut" "glutJoystickFunc" (:void *lpfn)) ;; (*func)(unsigned int buttonMask, int x, int y, int z), int pollInterval);

#| glut color index sub-api. |#
(defun-ffx :void "glut" "glutSetColor" (:int cell glfloat red glfloat green glfloat blue))
(defun-ffx glfloat "glut" "glutGetColor" (:int ndx :int component))
(defun-ffx :void "glut" "glutCopyColormap" (:int win))

#| glut state retrieval sub-api. |#
(defun-ffx :int "glut" "glutGet" (glenum e-what))
(defun-ffx :int "glut" "glutDeviceGet" (glenum type))

#| glut extension support sub-api |#
(defun-ffx :int "glut" "glutExtensionSupported" (:cstring name))
(defun-ffx :int "glut" "glutGetModifiers" ())
(defun-ffx :int "glut" "glutLayerGet" (glenum type))

(defun-ffx :void "glut" "glutBitmapCharacter" (:void *font :int character))

(defun-ffx :int "glut" "glutBitmapWidth" (:void *font :int character))
(defun-ffx :int "glut" "glutBitmapHeight" (:void *font))
;;;(defun-ffx glfloat "glut" "glutBitmapXOrig" (:void *font))
;;;(defun-ffx glfloat "glut" "glutBitmapYOrig" (:void *font))

(defun-ffx :void "glut" "glutStrokeCharacter" (:void *font :int character))
;;;(DEF-FUNCTION ("glutStrokeCharacter" GLUTSTROKECHARACTER)
;;;                     ((*FONT (* :VOID)) (CHARACTER :INT)) :RETURNING :VOID :MODULE "glut")
;;;(CFFI:DEFCFUN ("glutStrokeCharacter" GLUTSTROKECHARACTER) :VOID (*FONT :POINTER) (CHARACTER :INT))
;;;(defun-ffx glfloat "glut" "glutStrokeDescent" (:void *font))

#+(or)
(list
 (glut-bitmap-height glut_bitmap_times_roman_24)
 (glut-bitmap-width glut_bitmap_times_roman_24 (char-code #\h)))

(defun-ffx :int "glut" "glutStrokeWidth" (:void *font :int character))
(defun-ffx glfloat "glut" "glutStrokeHeight" (:void *font))

#+(or)
(list
 (glut-stroke-height glut_stroke_mono_roman)
 (glut-stroke-width glut_stroke_roman (char-code #\h)))

(defun-ffx :int "glut" "glutBitmapLength" (:void *font :cstring string))
(defun-ffx :int "glut" "glutStrokeLength" (:void *font :cstring string))

#| glut pre-built models sub-api |#

(defun-ffx :void "glut" "glutWireSphere" (gldouble radius glint slices glint stacks))
(defun-ffx :void "glut" "glutSolidSphere" (gldouble radius glint slices glint stacks))
(defun-ffx :void "glut" "glutWireCone" (gldouble base gldouble height glint slices glint stacks))
(defun-ffx :void "glut" "glutSolidCone" (gldouble base gldouble height glint slices glint stacks))
(defun-ffx :void "glut" "glutWireCube" (gldouble size))
(defun-ffx :void "glut" "glutSolidCube" (gldouble size))
(defun-ffx :void "glut" "glutWireTorus" (gldouble inner-radius gldouble outer-radius glint sides glint rings))
(defun-ffx :void "glut" "glutSolidTorus" (gldouble inner-radius gldouble outer-radius glint sides glint rings))
(defun-ffx :void "glut" "glutWireDodecahedron" ())
(defun-ffx :void "glut" "glutSolidDodecahedron" ())
(defun-ffx :void "glut" "glutWireTeapot" (gldouble size))
(defun-ffx :void "glut" "glutSolidTeapot" (gldouble size))
(defun-ffx :void "glut" "glutWireOctahedron" ())
(defun-ffx :void "glut" "glutSolidOctahedron" ())
(defun-ffx :void "glut" "glutWireTetrahedron" ())
(defun-ffx :void "glut" "glutSolidTetrahedron" ())
(defun-ffx :void "glut" "glutWireIcosahedron" ())
(defun-ffx :void "glut" "glutSolidIcosahedron" ())
(defun-ffx :void "glut" "glutWireRhombicDodecahedron" ( ))
(defun-ffx :void "glut" "glutSolidRhombicDodecahedron" ( ))
(defun-ffx :void "glut" "glutWireSierpinskiSponge" ( :int num_levels gldouble *offset-3 gldouble scale ))
(defun-ffx :void "glut" "glutSolidSierpinskiSponge" ( :int num_levels gldouble *offset-3 gldouble scale ))

