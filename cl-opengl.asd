;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

;(declaim (optimize (debug 2) (speed 1) (safety 1) (compilation-speed 1)))
;(declaim (optimize (debug 3) (speed 3) (safety 1) (compilation-speed 0)))


(in-package :asdf)

#-(or openmcl sbcl cmu clisp lispworks ecl allegro cormanlisp)
(error "Sorry, this Lisp is not yet supported.  Patches welcome!")

(defsystem cl-opengl
  :name "cl-opengl"
  :author "Kenny Tilton <ktilton@nyc.rr.com>"
  :version "1.0.0"
  :maintainer "Kenny Tilton <ktilton@nyc.rr.com>"
  :licence "MIT"
  :description "Partial OpenGL Bindings"
  :long-description "Bindings to most of OpenGL, more on demand"
  :perform (load-op :after (op cl-opengl)
             (pushnew :cl-opengl cl:*features*))
  :depends-on (:hello-cffi)
  :serial t
  :components ((:file "cl-opengl")
               (:file "gl-def" :depends-on ("cl-opengl"))
               (:file "gl-constants" :depends-on ("gl-def"))
               (:file "gl-functions" :depends-on ("gl-def"))
               (:file "glu-functions" :depends-on ("gl-def"))
               (:file "glut-loader" :depends-on ("cl-opengl"))
               (:file "glut-functions" :depends-on ("glut-loader"))
               (:file "glut-def" :depends-on ("glut-loader"))
               (:file "glut-extras" :depends-on ("glut-loader"))
               (:file "ogl-macros" :depends-on ("gl-def"))
               (:file "ogl-utils" :depends-on ("ogl-macros"))
               (:file "nehe-14" :depends-on ("ogl-macros"))))
