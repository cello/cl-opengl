;; -*- mode: Lisp; Syntax: Common-Lisp; Package: cl-opengl; -*-
;;;
;;; Copyright � 1995,2003 by Kenneth William Tilton.
;;;
;;; Permission is hereby granted, free of charge, to any person obtaining a copy 
;;; of this software and associated documentation files (the "Software"), to deal 
;;; in the Software without restriction, including without limitation the rights 
;;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
;;; copies of the Software, and to permit persons to whom the Software is furnished 
;;; to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in 
;;; all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
;;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
;;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
;;; IN THE SOFTWARE.

(in-package :cl-opengl)


(defconstant wcx 640)        ;; Window Width
(defconstant wcy 480)        ;; Window Height
(defparameter g-rot 0.0f0)



(defparameter *disp-ct* 0)
(defvar *working-objects*)

(ff-defun-callable  :cdecl :void mgwclose ()
  (print "closing callback entered"))

#+nextttt
(defun lesson-14 (&optional (dispfunc 'nh14disp))
  (declare (ignorable dispfunc))
  (setf *disp-ct* 0
    *working-objects* (make-hash-table))
  
  (progn ;; with-open-file (*standard-output* "/0dev/nh14.log" :direction :output :if-exists :new-version)
    (let ((*gl-begun* nil))
      (cl-glut-init)
      (glutsetoption +glut-action-on-window-close+ +glut-action-glutmainloop-returns+)
      
      (glutinitdisplaymode (+ +glut-rgb+ +glut-double+)) ;; Display Mode (Rgb And Double Buffered)
      (glutinitwindowsize 640 480)   ;; Window Size If We Start In Windowed Mode
      
      (let ((key "NeHe's OpenGL Framework"))
        (uffi:with-cstring (key-native key)
          (glutcreatewindow key-native)))
      
      ;(init) ;                                          // Our Initialization
      ;; Set up the callbacks in OpenGL/GLUT
      (glutdisplayfunc (ff-register-callable dispfunc))
      (glutwmclosefunc (ff-register-callable 'mgwclose))
      (glutkeyboardfunc (ff-register-callable 'mgwkey))
      (glmatrixmode gl_projection)
      (glloadidentity)
      (gluperspective 70d0 1d0 1d0 1000d0)
      (glulookat 0d0 0d0 5d0 0d0 0d0 0d0 0d0 1d0 0d0)
      
      (glmatrixmode gl_modelview)
      (glloadidentity)
      
      
      (glcleardepth 1d0)
      (glutmainloop)
      #+(or) (do ()
          ((zerop (glutgetwindow)))
        ;;(format t "before main loop ~a | ~&" (glutgetwindow))
        (glutmainloopevent)
        (sleep 0.08)
        ))))

#+(or)
(lesson-14)

(ff-defun-callable :cdecl :void mgwkey ((k :int) (x :int) (y :int))
  (print 'bingo)
  (mgwkeyi k x y))

(defun mgwkeyi (k x y)
  (declare (ignorable k x y))
  (print (list "mgwkey" k x y (glutgetwindow)))
  (let ((mods (glutgetmodifiers)))
    (declare (ignorable mods))
    (print (list :keyboard k mods x  y (code-char (logand k #xff))#+(or)(glut-get-window)))
    (case (code-char (logand k #xff))
      (#\escape (progn
                  (print (list "destroying window" (glutgetwindow) )
                    )
                  (glutDestroyWindow (glutgetwindow)))))))

(ff-defun-callable :cdecl :void nh14disp ()
  (nh14-disp))

#+nexttttt
(defun nh14-disp ()
  (glloadidentity)						;; Reset The Current Modelview Matrix
  
  (glclearcolor 0.0 0.0 0.0  0.5)
  (glclear (+ gl_color_buffer_bit gl_depth_buffer_bit))
       
  (glTranslatef 0.0f0 0.0f0 2.0f0)			;; Move Into The Screen

  ;;(font-glut-preview)

  (glRotatef g-rot 1.0f0 0.0f0 0.0f0)			;; Rotate On The X Axis
  (glRotatef (* g-rot 1.5f0) 0.0f0 1.0f0 0.0f0)	;; Rotate On The Y Axis
  (glRotatef (* g-rot 1.4f0) 0.0f0 0.0f0 1.0f0)	;; Rotate On The Z Axis
  (glScalef 0.002  0.003  0.002)

  ;; Pulsing Colors Based On The Rotation
  (glcolor3f (* 1.0f0 (cos (/ g-rot 20.0f0)))
    (* 1.0f0 (sin (/ g-rot 25.0f0)))
    (- 1.0f0 (* 0.5f0 (cos (/ g-rot 17.0f0)))))

  (with-matrix ()
    (gllinewidth 3f0)
    (let ((x (format nil "NeHe - ~a" (/ g-rot 50.0))))
      (with-cstring (msg x)
        (glutstrokestring glut_stroke_roman msg))))
  

  (progn
    (gllinewidth 1f0)
    (glutwireteapot 1000d0))

  (incf g-rot 0.10)

  (glutswapbuffers)
  (glutPostRedisplay)
  )

