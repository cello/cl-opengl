;; -*- mode: Lisp; Syntax: Common-Lisp; Package: cl-opengl; -*-
;;;
;;; Copyright � 1995,2003 by Kenneth William Tilton.
;;;
;;; Permission is hereby granted, free of charge, to any person obtaining a copy 
;;; of this software and associated documentation files (the "Software"), to deal 
;;; in the Software without restriction, including without limitation the rights 
;;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
;;; copies of the Software, and to permit persons to whom the Software is furnished 
;;; to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in 
;;; all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
;;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
;;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
;;; IN THE SOFTWARE.


(in-package :cl-opengl)

(eval-when (compile eval load)
  (export '(ffi-glut-id glut-callback-set glut-callbacks-set cl-glut-init xfg)))

#+dead?
(defun xfg ()
  #+allegro
  (dolist (lib '("freeglut" "glu32" "opengl32"))
    (dolist (dll (ff:list-all-foreign-libraries))
      (when (search lib (pathname-name dll))
        (print `(unloading foreign library ,dll))
        (setf *glut-dll* nil *opengl-dll* nil)
        (ff:unload-foreign-library dll)))))

(defparameter *mg-glut-display-busy* nil)

(defun cl-glut-init ()
  (let ((glut-state (glutget (coerce +glut-init-state+ 'integer))))
    (format t "~&cl-glut-init > glut state ~a" glut-state)
    (if (zerop glut-state)
        (progn
          (print "about to initialize")
          (let ((argc (fgn-alloc :int 1 :glut-init)))
            (setf (eltf argc 0) 0)
            (unwind-protect
                (progn
                  (glutInit argc (make-null-pointer '(:array :cstring)))
                  (print "glut initialised")
                  )
              (fgn-free argc))))
      (print "Glut already initialized"))
    (setf *mg-glut-display-busy* nil)))

(defvar *mdepth*)
(defvar *selecting*)

(defun closed-stream-p (w)
  (declare (ignore w))
  (let ((w (glutgetwindow)))
    (or (not (zerop (glgeterror)))
      (zerop w))))

(let ((mm (ffx:allocate-foreign-object :int 1)))
  (defun get-matrix-mode ()
    (glgetintegerv +gl-matrix-mode+ mm)
    (ffx:deref-array mm '(:array :int) 0)))

(let ((mm (ffx:allocate-foreign-object :int 1))
      (sd (ffx:allocate-foreign-object :int 1)))
  (defun get-stack-depth ()
    (glgetintegerv +gl-matrix-mode+ mm)
    (let ((mmi (ffx:deref-array mm '(:array :int) 0)))
      (glgetintegerv
       (cond
        ((eql mmi +gl-modelview+) +gl-modelview-stack-depth+)
        ((eql mmi +gl-projection+) +gl-projection-stack-depth+)
        ((eql mmi +gl-texture+) +gl-texture-stack-depth+)
        (t (break "bad matrix")))
       sd)
      (ffx:deref-array sd '(:array :int) 0))))

(defun cello-matrix-mode (&optional (tag :anon))
  (let ((mm (ffx:allocate-foreign-object :int 1))
        )
    (glgetintegerv +gl-matrix-mode+ mm)
    (let ((mmi (ffx:deref-array mm '(:array :int) 0)))
      (unwind-protect
          (cond
           ((eql mmi +gl-modelview+) :model-view)
           ((eql mmi +gl-projection+) :projection)
           ((eql mmi +gl-texture+) :texture)
           
           (t (break "gl-stack-depth> unexpected matrix mode ~a ~a" tag mmi)))
        (ffx:free-foreign-object mm)))))


(defun glut-callback-set (setter settee)
  (when settee
     (funcall setter (ff-register-callable settee))))

(defun ffi-glut-id (id)
  (make-ff-pointer id))

(defun glut-callbacks-set (&key close
                           display
                           idle
                           keyboard
                           reshape
                           entry
                           visibility
                           timer-info
                           passive-motion
                           motion
                           mouse
                           menu-state
                           special
                           button-box
                           dials
                           menu-status
                           overlay-display
                           window-status
                           keyboard-up
                           special-up
                           joystick)
  (progn
    (glut-callback-set 'glut-close-func close)
    (glut-callback-set 'glut-display-func display)
    (glut-callback-set 'glut-idle-func idle)
    
    (glut-callback-set 'glut-keyboard-func keyboard)
    (glut-callback-set 'glut-reshape-func reshape)
    (glut-callback-set 'glut-entry-func entry)
    (glut-callback-set 'glut-visibility-func visibility)
    (glut-callback-set 'glut-passive-motion-func passive-motion)
    (glut-callback-set 'glut-motion-func motion)
    (glut-callback-set 'glut-mouse-func mouse)
    (glut-callback-set 'glut-menu-state-func menu-state)
    (glut-callback-set 'glut-special-func special)
    (glut-callback-set 'glut-button-box-func button-box)
    (glut-callback-set 'glut-dials-func dials)
    (glut-callback-set 'glut-menu-status-func menu-status)
    (glut-callback-set 'glut-overlay-display-func overlay-display)
    (glut-callback-set 'glut-window-status-func window-status)
    (glut-callback-set 'glut-keyboard-up-func keyboard-up)
    (glut-callback-set 'glut-special-up-func special-up)
    (glut-callback-set 'glut-joystick-func joystick)
    )
  (when timer-info
    (apply #'glut-timer-func timer-info)))