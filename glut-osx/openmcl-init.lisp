(load "/opt/common-lisp/asdf/asdf.lisp")
(defparameter cl-user::*devel-root*
  (make-pathname :directory '(:absolute "data" "projects" "cell-cultures")))
(load "/users/kennetht/tools-init.lisp")
(load "/data/projects/cell-cultures/build.lisp")
