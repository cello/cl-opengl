;;; Example openmcl FFI by hamlink
(in-package :cl-opengl)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (ffx::ff-load-library "GLUT.framework/GLUT"
                        :force-load (or #-lispworks t)
                        :module "glut"))

(let ((glut-initialized-p nil))
  (defun initialize-glut ()
    (let ((command-line-strings (list "openmcl")))
      (when (not glut-initialized-p)
        (ccl::with-string-vector (argv command-line-strings)
          (ccl::rlet ((argvp (* t))    ; glutinit takes (* (:signed 32)) and (* (* (:unsigned 8)))
                      (argcp :signed)) ; so why are these declared as (* t) and :signed?
	    (setf (ccl::%get-long argcp) (length command-line-strings)
		  (ccl::%get-ptr argvp) argv)
	    (glut-Init argcp argvp)))
	(setf glut-initialized-p t))))
  ;; When a saved image is restarted, it needs to know that glut
  ;; hasn't been initialized yet.
  (defun uninitialize-glut ()
    (setf glut-initialized-p nil))
  )


(defparameter *matrix-mode* GL_MODELVIEW)
(defmacro with-matrix-mode (mode &body body)
  `(unwind-protect
       (let ((*matrix-mode* ,mode))
	 (glMatrixMode *matrix-mode*)
	 ,@body)
     (glMatrixMode *matrix-mode*)))

(defparameter *glut-process* nil)

(eval-when (load compile eval)
  (export '(glut-application glut-app-initialize glut-do-keyboard
            glut-window-factory glut-make-window
            glut-window glut-window-callbacks-initialize
            glut-do-keyboard glut-do-reshape glut-do-display
            glut-do-close
            start-time time-now)))

(defclass glut-application ()())

(defmethod glut-app-initialize (app)
  (declare (ignore app)))

(defmethod glut-make-window (app)
  (declare (ignore app))
  (make-instance 'glut-window))

(defun now ()
  (/ (get-internal-real-time) internal-time-units-per-second))

(defclass glut-window ()
  ((start-time :accessor start-time :initarg :start-time
               :initform (now))
   (time-now :accessor time-now :initarg :time-now
               :initform (now))))

(ff-defun-callable :cdecl :void glut-do-keyboard-callback (:int k :int x :int y)
  (let ((w (glut-find-window (glut-get-window))))
    (setf (time-now w) (now))
    (glut-do-keyboard w k x y)))

(defmethod glut-do-keyboard (win k x y &aux (id (glut-get-window)))
  (declare (ignorable win k x y))
  (print (list "mgwkey" k x y id))
  (let ((mods (glut-get-modifiers)))
    (declare (ignorable mods))
    (print (list :keyboard k mods x  y (code-char (logand k #xff))))
    (case (code-char (logand k #xff))
      (#\escape (progn
                  (print (list "destroying window" id )
                         )
                  (glut-destroy-window id))))))

(ff-defun-callable :cdecl :void glut-do-reshape-callback (:int x :int y)
  (let ((w (glut-find-window (glut-get-window))))
    (setf (time-now w) (now))
    (glut-do-reshape w x y)))

(defmethod glut-do-reshape (win x y)
  (declare (ignorable win x y)))

;;; --- close ---------------------------

(ff-defun-callable :cdecl :void glut-do-close-callback ()
  (let ((w (glut-find-window (glut-get-window))))
    (setf (time-now w) (now))
    (glut-do-close w)))

(defmethod glut-do-close (win)
  (declare (ignorable win))
  (glut-destroy-window (glut-get-window)))

;;; ---display -------------------------

(ff-defun-callable :cdecl :void glut-do-display-callback ()
  (let ((w (glut-find-window (glut-get-window))))
    (setf (time-now w) (now))
    (glut-do-display w)))

(defmethod glut-do-display (win)
  (declare (ignorable win)))

#+darwin
(let ((process nil)
      application ;; latest app (vestigial?)
      applications ;; allow diff types
      windows
      new-window-needed-p)

  (defun glut-app-initialized-p (app)
    (or (find app applications)
        (let ((existing (find (class-of app) applications :key 'class-of)))
          (assert (not existing) ()
                  "Not ready for diff instances ~a of same app class ~a in one run"
                  (list existing app) (class-of app)))))

  (defun glut-get-process () process)

  (defun glut-find-window (id)
    (cdr (assoc id windows)))

  (defun glut-window-register (id w)
    (push (cons id w) windows))

  (defun glut-window-initialize (w)
    (glut-window-register (glut-get-window) w)
    (glut-window-callbacks-initialize w)
    (glut-show-window)) ;; >>> parameterize this

  (defmethod glut-window-callbacks-initialize (w)
    ;;
    ;; callbacks themselves can be overridden, but this
    ;; hook allows a window to decide /which/ callbacks
    ;; get set up. Could be a mask instead.
    ;;
    (declare (ignore w))
    (glut-display-func (ff-register-callable
                        'glut-do-display-callback))
    (glut-reshape-func (ff-register-callable
                        'glut-do-reshape-callback))
    (glut-keyboard-func (ff-register-callable
                         'glut-do-keyboard-callback))
    (glut-wm-close-func (ff-register-callable
                         'glut-do-close-callback)))
                            
  (defun glut-app-window-trigger (app)
    (unless (glut-app-initialized-p app)
      (glut-app-initialize app))
    (setf application app
          new-window-needed-p t))

  (defun glut-app-register (app rerun-key)
    (assert (null (glut-app-lookup rerun-key)) ()
            "Application key ~a already registered for ~a"
            rerun-key (glut-app-lookup rerun-key))
    (push (cons rerun-key app) applications)
    app)

  (defun glut-app-lookup (rerun-key)
    (cdr (assoc rerun-key applications)))

  (defun glut-rerun (rerun-key
                     &aux (app (if rerun-key
                                   (glut-app-lookup rerun-key)
                                   application)))
    (if rerun-key
        (assert app () "glut-rerun: no prior app matches rerun key ~a"
                rerun-key)
        (assert app () "glut-rerun: no prior application to rerun"))
    (glut-app-window-trigger app))

  (defun glut-run (app &optional (rerun-key (type-of app)))
    (glut-app-register app rerun-key)
    (if process
        (glut-app-window-trigger app)
        (setf process
              (ccl:process-run-function
               "OpenGL main thread"
               #'(lambda ()
                   (print `(process is ,ccl:*current-process*))
                   
                   (ccl::external-call "_CFRunLoopGetCurrent" :address)
                   (ccl::external-call "__CFRunLoopSetCurrent"
                                       :address (ccl::external-call "_CFRunLoopGetMain" :address))
                   (ccl::%stack-block ((psn 8))
                     (ccl::external-call "_GetCurrentProcess" :address psn)
                     (ccl::with-cstrs ((name "simple OpenGL example"))
                       (ccl::external-call "_CPSSetProcessName" :address psn :address name)))
                   
                   (initialize-glut)
                   (glut-app-window-trigger app)

                   (loop do
                        (when new-window-needed-p
                          (glut-window-initialize
                           (glut-make-window application))
                          (setf new-window-needed-p nil))
                        (glut-check-loop))))))))


