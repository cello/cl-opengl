(in-package  :cl-ftgl)

(defclass ftgl-app (ogl::glut-application)())

(defclass font-manager ()
  ((font-cache :initarg :font-cache :initform nil :accessor font-cache)))

(defclass ftgl-win (ogl::glut-window font-manager)
  ((rot :initform 0 :accessor rot)
   (rot-delta :initform 0.40 :accessor rot-delta)))

(defun gogo-ftgl ()
  (ogl::glut-run
   (make-instance 'ftgl-app) :ftgl))

#+test (cl-ftgl::gogo-ftgl)
#+test (ogl::glut-rerun :ftgl) ;; to get second window from same app

(defmethod glut-make-window ((factory ftgl-app))
  (glut-Init-Display-Mode (logior GLUT_RGB GLUT_DOUBLE))
  (glut-Init-Window-Size 500 500)
  (glut-Init-Window-Position 0 0)
  (ccl::with-cstrs ((title  "Simple FTGL Example"))
    (glut-Create-Window title))

  (make-instance 'ftgl-win
   :font-cache  (mapcar (lambda (mode)
                          (cons mode (ftgl-make mode *gui-style-default-face* 36 96 18)))
                        '(:bitmap 
                          :pixmap 
                          :polygon 
                          :outline 
                          :texture 
                          :extruded))))
    
#+test (cl-ftgl::gogo)


(defmethod glut-do-display ((win ftgl-win))
  (flet ((test-font (id)
           (cdr (assoc id (font-cache win)))))
    (ogl::with-matrix-mode GL_MODELVIEW
      (gl-load-identity)
      (gl-clear-color 0.0 0.0 0.0 1.0)
      
      (gl-clear (+ GL_COLOR_BUFFER_BIT GL_DEPTH_BUFFER_BIT))
      
      ;; --- you are here ---
      (gl-color3f 1.0 1.0 0.0)
      (gl-rectf -0.01 0.01 0.01 -0.01)
      
      ;; --- pulsating color ---
      (gl-color3f (* 1.0f0 (cos (/ (rot win) 20.0f0)))
                  (* 1.0f0 (sin (/ (rot win) 25.0f0)))
                  (- 1.0f0 (* 0.5f0 (cos (/ (rot win) 17.0f0)))))
      (incf (rot win) (rot-delta win))
      
      (gl-scalef 0.003  0.003  0.0)
      (gl-disable gl_lighting)
      
      ;; --- bitmap ---
      (gl-raster-pos3i 10 -30 0)
      (ftgl-render (test-font :bitmap) "un-rotated bitmap")
      
      ;; --- pixmap ---
      (gl-raster-pos3i 10 10 0)
      (ftgl-render (test-font :pixmap) "un-rotated pixmap")
      
      ;; --- pixmap ---
      (gl-raster-pos3i 60 -120 0)
      (ftgl-render (test-font :pixmap)
                   (format nil "fps=~d"
                           (floor (/ (rot win) (rot-delta win))
                                  (max 1 (- (time-now win) (start-time win))))))
      
      ;; --- polygon ---
      (with-matrix ()
        (gl-translatef -100 100 0)
        (ftgl-render (test-font :polygon) "un-rotated polygon"))
      
      ;; --- outline ---
      (with-matrix ()
        (gl-translatef -100 50 0)
        (gl-line-width  3)
        (ftgl-render (test-font :outline) "un-rotated outline"))
      
      ;; --- extruded polygon ---
      (with-matrix ()
        (gl-polygon-mode gl_front_and_back gl_line)
        (gl-rotatef (rot win) 1.0f0 0.5f0 0.0f0)
        (gl-scalef 5 5 5)
        (gl-translatef -70 -20 0)
        (gl-line-width 1)
        (ftgl-render (test-font :extruded) "NeHe")
        (gl-polygon-mode gl_front_and_back gl_fill))
      
    ;;; --- texture ---
    (with-matrix ()
        (gl-rotatef (rot win) 1.0f0 0.0f0 0.0f0)
        (gl-rotatef (* (rot win) 1.5f0) 0.0f0 1.0f0 0.0f0)
        (gl-rotatef (* (rot win) 1.4f0) 0.0f0 0.0f0 1.0f0)
        (gl-enable gl_texture_2d)
        (gl-disable gl_lighting)
        (ftgl-render (test-font :texture) "NeHe Lesson 14"))
      )

    (glut-swap-buffers)
    (glut-post-redisplay)))

#+test (cl-ftgl::gogo)

