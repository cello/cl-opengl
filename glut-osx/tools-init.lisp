;;; ====================================================================
;;;  - AUTOLOAD -
;;; ====================================================================

(defparameter *lisp-stuff*
  (make-pathname :directory '(:absolute "opt" "common-lisp")))

(defparameter *site-stuff*
  (merge-pathnames 
   (make-pathname :directory '(:relative "asdf-packages" "site-systems"))
   *lisp-stuff*))

#-:asdf-install
(progn
  (push (merge-pathnames  (make-pathname :directory '(:relative "asdf-install"))
                          *site-stuff*)
        asdf:*central-registry*)
  (asdf:operate 'asdf::load-op :asdf-install))

#-:uffi
(progn
  (pushnew (merge-pathnames  (make-pathname :directory '(:relative "uffi"))
                             *site-stuff*)
           asdf:*central-registry*)
  (asdf:operate 'asdf::load-op :uffi))

;;; Note that his script assumes there exists a special
;;; variable called *devel-root* which points to the 
;;; directory containg the cell-cultures directory

(load (merge-pathnames
       (make-pathname 
        ;;:directory '(:relative "cell-cultures")
        :name "build-sys-kt" :type "lisp")
       *devel-root*))

(load (merge-pathnames
       (make-pathname 
       ;; :directory '(:relative "cell-cultures")
        :name "build" :type "lisp")
       *devel-root*))

(load "/devl/glut-execute.lisp")

;;; ====================================================================
;;;  - END OF FILE -
;;; ====================================================================