(in-package :cl-magick)

(defconstant wcx 640)        ;; Window Width
(defconstant wcy 480)        ;; Window Height

#+test
(probe-file
 (demo-image-file 'shapers "grace.jpg"))

(defclass mgk-app (ogl::glut-application)())

(defclass mgk-win (ogl::glut-window wand-manager)
  ((john :accessor john :initform nil)
   (grace :accessor grace :initform nil)
   (rotx :initform 0 :accessor rotx)
   (roty :initform 0 :accessor roty)
   (rotz :initform 0 :accessor rotz)))

(defun gogo-mgk ()
  (ogl::glut-run (make-instance 'mgk-app) :mgk))

#+test (cl-magick::gogo-mgk)
#+test2 (ogl::glut-rerun :mgk)

(defmethod glut-make-window ((factory mgk-app))
  (glut-init-display-mode (+ glut_rgb glut_double))
  (glut-init-window-size wcx wcy)   ;; Window Size If We Start In Windowed Mode
    
  (Let ((key "cl-magick demo"))
    (ccl::with-cstrs ((key-native key))
      (glut-create-window key-native)))
    
  (gl-enable gl_texture_2d)
  (gl-shade-model gl_smooth)
  (gl-clear-color 0 0 0 1)
  (gl-clear-depth 1)
  (gl-enable gl_depth_test)
  (gl-depth-func gl_lequal)
  (gl-hint gl_perspective_correction_hint gl_nicest)
  
  (let ((gw (make-instance 'mgk-win)))
    (setf (john gw) (mgk:wand-ensure-typed gw
                     'wand-texture
                     (demo-image-file 'shapers "jmcbw512.jpg"))
          (grace gw) (mgk:wand-ensure-typed gw
                      'wand-pixels
                      (demo-image-file 'shapers "grace.jpg")))

    (magick-test-reshape wcx wcy)
    gw))



#+test (cl-magick::gogo)
#+test (ogl::glut-new-window)

#+test
(progn
  (cl-magick-init)
  (path-to-wand (merge-pathnames
                 (make-pathname :name "metal" :type "gif")
                 *magick-wand-templates*)))
 
(defun demo-image-subdir (subdir)
  (make-pathname :directory `(:absolute "data" "projects" "cell-cultures"
                                         "GRAPHICS" ,(string subdir))))

(defun demo-image-file (subdir file)
  (merge-pathnames file
    (demo-image-subdir subdir)))

#+test (cl-magick::gogo)

(defmethod glut-do-reshape ((win mgk-win) w h)
  (magick-test-reshape w h))

(defun magick-test-reshape (width height)
  (unless (or (zerop width) (zerop height))
    (gl-viewport 0 0 width height)
    (gl-matrix-mode gl_projection)
    (gl-load-identity)
    (glu-perspective 45 (/ width height) 0.1 100)
    (gl-matrix-mode gl_modelview)
    (gl-load-identity)))


(defmethod glut-do-display ((win mgk-win))
  (gl-load-identity)
  (gl-clear (+ gl_color_buffer_bit gl_depth_buffer_bit))
  ;--------------------------------------------
  
  (let ((ogl::*gl-begun* nil))
    (gl-translatef 0 0 -5) 
    
    (gl-rotatef (incf (rotx win) 0.3) 1.0 0.0 0.0)
    (gl-rotatef (incf (roty win) 0.2) 0.0 1.0 0.0)
    (gl-rotatef (incf (rotz win) 0.4) 0.0 0.0 1.0)
    
    (wand-texture-activate (john win))
    
    (flet ((v3f (x y z)
             (let ((scale 1))
               (gl-vertex3f (* scale x)(* scale y)(* scale z)))))
      (with-gl-begun (gl_quads)
        ;; Front Face
        (gl-tex-coord2f 0 0)(v3f -1 -1  1)
        (gl-tex-coord2f 1 0)(v3f  1 -1  1)
        (gl-tex-coord2f 1 1)(v3f  1  1  1)
        (gl-tex-coord2f 0 1)(v3f -1  1  1)
        
        ;; Back Face
        (gl-tex-coord2f 1 0) (v3f -1 -1 -1)
        (gl-tex-coord2f 1 1) (v3f -1  1 -1)
        (gl-tex-coord2f 0 1) (v3f  1  1 -1)
        (gl-tex-coord2f 0 0) (v3f  1 -1 -1)
        ;;;   Top Face
        (gl-tex-coord2f 0 1) (v3f -1  1 -1)
        (gl-tex-coord2f 0 0) (v3f -1  1  1)
        (gl-tex-coord2f 1 0) (v3f  1  1  1)
        (gl-tex-coord2f 1 1) (v3f  1  1 -1)
        ;;;   Bottom Face
        (gl-tex-coord2f 1 1) (v3f -1 -1 -1)
        (gl-tex-coord2f 0 1) (v3f  1 -1 -1)
        (gl-tex-coord2f 0 0) (v3f  1 -1  1)
        (gl-tex-coord2f 1 0) (v3f -1 -1  1)
        ;;;   Right face
        (gl-tex-coord2f 1 0) (v3f  1 -1 -1)
        (gl-tex-coord2f 1 1) (v3f  1  1 -1)
        (gl-tex-coord2f 0 1) (v3f  1  1  1)
        (gl-tex-coord2f 0 0) (v3f  1 -1  1)
        ;;;   Left Face
        (gl-tex-coord2f 0 0) (v3f -1 -1 -1)
        (gl-tex-coord2f 1 0) (v3f -1 -1  1)
        (gl-tex-coord2f 1 1) (v3f -1  1  1)
        (gl-tex-coord2f 0 1) (v3f -1  1 -1)
        ))
    #+frank-this-works-too (wand-render (grace win) 0 0 1 -1)
    )
  (glut-swap-buffers)
  (glut-post-redisplay)
  )