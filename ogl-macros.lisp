;; -*- mode: Lisp; Syntax: Common-Lisp; Package: cl-opengl; -*-
;;
;;;
;;; Copyright � 2004 by Kenneth William Tilton.
;;;
;;; Permission is hereby granted, free of charge, to any person obtaining a copy 
;;; of this software and associated documentation files (the "Software"), to deal 
;;; in the Software without restriction, including without limitation the rights 
;;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
;;; copies of the Software, and to permit persons to whom the Software is furnished 
;;; to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in 
;;; all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
;;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
;;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
;;; IN THE SOFTWARE.

(in-package :cl-opengl)

(defvar *stack-depth*
  (fgn-alloc :int 1 :ignore))

(defmacro with-matrix ((&optional load-identity-p) &body body)
  `(call-with-matrix ,load-identity-p (lambda () ,@body) ',body))


(defun call-with-matrix (load-identity-p matrix-fn matrix-code)
  (declare (ignorable matrix-code))
  (glPushMatrix)  
  (unwind-protect
      (progn
        (when load-identity-p
          (glLoadIdentity))
        (funcall matrix-fn))
    (glpopmatrix)))


(defparameter *matrix-mode* gl_modelview)
(defmacro with-matrix-mode (mode &body body)
  `(unwind-protect
       (let ((*matrix-mode* ,mode))
	 (glMatrixMode *matrix-mode*)
	 ,@body)
     (glMatrixMode *matrix-mode*)))


#+debugversion
(defun call-with-matrix (load-identity-p matrix-fn matrix-code)
  (let ((mm-pushed (ogl::get-matrix-mode))
        (sd-pushed (ogl::get-stack-depth)))
     
    (glPushMatrix)
    (glec :with-matrix-push)
    (unwind-protect
        (progn
          (when (eql gl_modelview_matrix mm-pushed)
            (glgetintegerv gl_modelview_stack_depth *stack-depth*)
            (glec :get-stack-depth)
            (print `(with-matrix model matrix stack ,(aforef *stack-depth* 0))))
             
          (when load-identity-p
            (glLoadIdentity))
          (prog1
              (funcall matrix-fn)
            (glec :with-matrix)))
      (assert (eql mm-pushed (ogl::get-matrix-mode))()
        "matrix-mode left as ~a  instead of ~a by form ~a"
        (ogl::get-matrix-mode) mm-pushed  matrix-code)
      (glpopmatrix)
      (assert (eql sd-pushed (ogl::get-stack-depth))()
        "matrix depth deviated ~d during ~a"
        (- sd-pushed (ogl::get-stack-depth))
        matrix-code)
      (glec :exit-with-stack))))

(defmacro with-attrib ((&rest attribs) &body body)
  `(call-with-attrib
    ,(apply '+ (mapcar 'symbol-value attribs))
    (lambda () ,@body)))

(defun call-with-attrib (attrib-mask attrib-fn)
  (glpushattrib attrib-mask)
  (glec :with-attrib-push)
  (unwind-protect
      (prog1
          (funcall attrib-fn)
        (glec :with-attrib))
    (glpopattrib)
    ))

(defmacro with-client-attrib ((&rest attribs) &body body)
  `(call-with-client-attrib
    ,(apply '+ (mapcar 'symbol-value attribs))
    (lambda () ,@body)))

(defun call-with-client-attrib (attrib-mask attrib-fn)
  (glpushclientattrib attrib-mask)
  (glec :with-client-attrib-push)
  (unwind-protect
      (prog1
          (funcall attrib-fn)
        (glec :with-client-attrib))
    (glpopclientattrib)
    ))

(defvar *gl-begun*)
(defvar *gl-stop*)
(defmacro with-gl-begun ((what) &body body)
  `(progn
     (when *gl-begun*
       (setf *gl-stop* t)
       (break ":nestedbegin"))
     (let ((*gl-begun* t))
       (glbegin ,what)
       ,@body
       (glend)
       (glec :with-gl-begun))))

(defun glec (&optional (id :anon))
  (unless (and (boundp '*gl-begun*) *gl-begun*)
    (let ((e (glgeterror)))
      (if (zerop e)
          (unless t
            (print `(cool ,id)))
        (if t
            (unless (boundp '*gl-stop*)
              (setf *gl-stop* t)
              (format t "~&~%OGL error ~a at ID ~a" e id)
              ;(break "OGL error ~a at ID ~a" e id)
              )
          #+sigh (print `("OGL error ~a at ID ~a" ,e ,id)))))))

