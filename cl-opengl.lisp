;; -*- mode: Lisp; Syntax: Common-Lisp; Package: cl-opengl; -*-
;;
;;;
;;; Copyright � 2004 by Kenneth William Tilton.
;;;
;;; Permission is hereby granted, free of charge, to any person obtaining a copy 
;;; of this software and associated documentation files (the "Software"), to deal 
;;; in the Software without restriction, including without limitation the rights 
;;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
;;; copies of the Software, and to permit persons to whom the Software is furnished 
;;; to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in 
;;; all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
;;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
;;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
;;; IN THE SOFTWARE.

(pushnew :cl-opengl *features*)

(defpackage #:cl-opengl
  (:nicknames #:ogl)
  (:use #:common-lisp #:cffi #:ffx)
  (:export #:*ogl-listing-p*
    #:with-matrix #:with-matrix-mode
    #:with-attrib #:with-client-attrib
    #:with-gl-begun 
    #:gl-pushm 
    #:gl-popm
    #:cl-opengl-init 
    #:closed-stream-p 
    #:*selecting*
    #:cl-opengl-reset
    #:ogl-texture
    #:ncalc-normalf #:ncalc-normalfv #:ogl-get-int #:ogl-get-boolean 
    #:v3f #:make-v3f #:v3f-x #:v3f-y #:v3f-z
    #:with-gl-param #:xlin #:xlout
    #:ups #:ups-most #:ups-more #:downs #:downs-most #:downs-more #:farther #:nearer
    #:ogl-texture-delete #:ogl-texture-gen #:ogl-tex-gen-setup
    #:ogl-bounds #:ogl-scissor-box #:ogl-raster-pos-get
    #:ogl-pen-move #:with-bitmap-shifted
    #:texture-name
    #:eltgli #:ogl-tex-activate #:gl-name
    #:mgwclose #:freeg))

(in-package :cl-opengl)

(defparameter *selecting* nil)

(push (make-pathname
       :directory '(:absolute "0devtools" "cffi"))
  asdf:*central-registry*)

(push (make-pathname
       :directory '(:absolute "0devtools" "verrazano-support"))
  asdf:*central-registry*)

(defparameter *gl-dynamic-lib*
  #+(or win32 windows mswindows)
  (make-pathname
   ;; #+lispworks :host #-lispworks :device "C"
   :directory '(:absolute "windows" "system32")
   :name "opengl32"
   :type "dll")
  #+(or darwin unix powerpc)
  (make-pathname
    :directory '(:absolute "System" "Library" "Frameworks" 
                           "OpenGL.framework" "Versions" "Current")
    :name "OpenGL"
    :type nil))

(defparameter *glu-dynamic-lib*
  #+(or win32 windows mswindows)
    (make-pathname
    ;;; #+lispworks :host #-lispworks :device "C"
      :directory '(:absolute "windows" "system32")
      :name "glu32"
      :type "dll")
  #+(or darwin unix powerpc) 
  (make-pathname
    :directory '(:absolute "System" "Library" "Frameworks" 
                           "GLU.framework" "Versions" "Current")
    :name "GLU"
    :type nil))

(defvar *opengl-dll* nil)

(defun cl-opengl-load ()
  (declare (ignorable load-oglfont-p))
  (unless *opengl-dll*
    (print "loading open GL/GLU")
    (ffx:load-foreign-library (namestring *gl-dynamic-lib*)) ;  :module "open-gl")
    ;; -lispworks#-lispworks
    (setf *opengl-dll*
      (ffx:load-foreign-library
       (namestring *glu-dynamic-lib*)))))

(eval-when (load eval)
  (cl-opengl-load))

(defun gl-boolean-test (value)
  #+allegro (not (eql value #\null))
  #-allegro (not (zerop value)))

#+yeahyeah
(defun dump-lists (min max)
  (loop with start
        and end
        for lx from min to max
        when (gl-boolean-test (glislist lx))
        do (if start
               (if end
                   (if (eql lx (1+ end))
                       (setf end lx)
                     (print `(gl ,start to ,end)))
                 (if (eql lx (1+ start))
                     (setf end lx)
                   (print `(gl ,start))))
             (setf start lx))))


(dfenum storagetype
  char-pixel
  short-pixel
  integer-pixel
  long-pixel
  float-pixel
  double-pixel)

(dfenum filtertypes
  undefined-filter
  point-filter
  box-filter
  triangle-filter
  hermite-filter
  hanning-filter
  hamming-filter
  blackman-filter
  gaussian-filter
  quadratic-filter
  cubic-filter
  catrom-filter
  mitchell-filter
  lanczos-filter
  bessel-filter
  sinc-filter)