;; -*- mode: Lisp; Syntax: Common-Lisp; Package: cl-opengl; -*-
;;
;;;
;;; Copyright � 2004 by Kenneth William Tilton.
;;;
;;; Permission is hereby granted, free of charge, to any person obtaining a copy 
;;; of this software and associated documentation files (the "Software"), to deal 
;;; in the Software without restriction, including without limitation the rights 
;;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
;;; copies of the Software, and to permit persons to whom the Software is furnished 
;;; to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in 
;;; all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
;;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
;;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
;;; IN THE SOFTWARE.



(in-package :cl-opengl)

(defun ogl-tex-activate (tex-name)
  (assert tex-name)
  ;;(print `(ogl-tex-activate doing ,tex-name))
  (gl-bind-texture gl_texture_2d tex-name)
  (gl-enable gl_texture_2d)
  (gl-polygon-mode gl_front_and_back gl_fill) ;; just front?
  )

(defparameter *textures-1* (fgn-alloc 'gluint 1 :ignore))

(defun ogl-texture-delete (texture-name)
  ;;(print `(deleting-tx ,texture-name))
  (setf (ff-elt *textures-1* gluint 0) texture-name)
  (glfree :texture texture-name)
  (gl-delete-textures 1 *textures-1*))

(defun ogl-texture-gen ()
  (gl-gen-textures 1 *textures-1*)
  (glec :ogl-texture-gen)
  (ff-elt *textures-1* gluint 0))

(let (gl-s-plane gl-t-plane gl-r-plane gl-q-plane)
  (defun ogl-tex-gen-setup (mode tex-env tex-wrap scale &rest planes)
    ;;(ukt::trc nil "ogl-tex-gen-setup:" mode tex-env tex-wrap scale planes)
    (gl-tex-envf gl_texture_env gl_texture_env_mode tex-env)
    (gl-tex-parameterf gl_texture_2d gl_texture_min_filter gl_linear )
    (gl-tex-parameterf gl_texture_2d gl_texture_mag_filter gl_linear )
  
    (gl-tex-parameteri gl_texture_2d gl_texture_wrap_s tex-wrap) ; gl-repeat for tiling
    (gl-tex-parameteri gl_texture_2d gl_texture_wrap_t tex-wrap) ;--
    
    (loop for plane in planes
        do (ecase plane
             (:s  (gl-tex-geni gl_s gl_texture_gen_mode mode)
               (gl-tex-genfv gl_s gl_object_plane
                 (ff-floatv-ensure gl-s-plane scale 0 0 0))
               (gl-enable gl_texture_gen_s))
             (:tee  (gl-tex-geni gl_t gl_texture_gen_mode mode)
               (gl-tex-genfv gl_t gl_object_plane
                 (ff-floatv-ensure gl-t-plane 0 scale 0 0))
               (gl-enable gl_texture_gen_t))
             (:r  (gl-tex-geni gl_r gl_texture_gen_mode mode)
               (gl-tex-genfv gl_r gl_object_plane
                 (ff-floatv-ensure gl-r-plane 0 0 scale 0))
               (gl-enable gl_texture_gen_r))
             (:q  (gl-tex-geni gl_q gl_texture_gen_mode mode)
               (gl-tex-genfv gl_q gl_object_plane
                 (ff-floatv-ensure gl-q-plane 0 0 scale 0))
               (gl-enable gl_texture_gen_q))))))

(defun ogl-scissor-box ()
  (let ((box (fgn-alloc 'glint 4 :scissor)))
    (gl-get-integerv gl_scissor_box box)
    box))

(defun ogl-bounds (ff-box)
  (loop for n below 4 
      collecting (eltgli ff-box n)))

(defun ups (&rest values)
  (apply '+ values))

(defun ups-more (&rest values)
  (apply '> values))

(defun ups-most (&rest values)
  (apply 'max values))

(defun downs (&rest values)
  (apply '- values))

(defun downs-most (&rest values)
  (apply 'min values))

(defun downs-more (&rest values)
  (apply '< values))

(defun farther (&rest values)
  (apply '- values))
(defun xlin (&rest values) ;; yep. moves matrix, not object
  (apply '+ values))

(defun nearer (&rest values)
  (apply '+ values))
(defun xlout (&rest values) ;; yep. moves matrix, not object
  (apply '- values))

(defun ncalc-normalf(v0x v0y v0z v1x v1y v1z v2x v2y v2z
                      &aux d0x d0y d0z d1x d1y d1z)
  (setf d0x (- v1x v0x)
    d0y (- v1y v0y)
    d0z (- v1z v0z))

  (setf d1x (- v2x v1x)
    d1y (- v2y v1y)
    d1z (- v2z v1z))

  (xgl-normalize-v3f  
   (- (* d0y d1z) (* d0z d1y))
   (- (* d0z d1x) (* d0x d1z))
   (- (* d0x d1y) (* d0y d1x))))


(defstruct v3f
  (x 0)(y 0)(z 0))

(defun xgl-normalize-v3f (x y z)
  (let ((m2 (+ (* x x) (* y y) (* z z))))
    (if (zerop m2)
        (values x y z)
      (let ((m (sqrt m2)))
        ;(trc "normalizing div" m)
        ;;(cells::count-it :normalize-3f)
        (values (+ (/ x m)) (+ (/ y m)) (+ (/ z m)))))))

(defparameter *ogl-boolean*
  (fgn-alloc 'glboolean 1 :ignore))

(defun ogl-get-boolean (gl-code)
  (gl-get-booleanv gl-code *ogl-boolean*)
  (not (zerop (mem-aref *ogl-boolean* 'glboolean 0))))

(defparameter *ogl-int*
  (fgn-alloc 'glint 1 :ignore))

(defparameter *ogl-float-1*
  (fgn-alloc 'glfloat 1 :ignore))

(defun wrap-float (lisp-float-value)
  (setf (mem-aref *ogl-float-1* 'glfloat 0) (* 1.0f0 lisp-float-value))
  *ogl-float-1*)

(defun eltgli (v n)
  (ff-elt v glint n))

(defun ogl-get-int (gl-code)
  (gl-get-integerv gl-code *ogl-int*)
  (eltgli *ogl-int* 0))

(defparameter *dbg-viewport-r*
  (fgn-alloc 'glint 4 :ignore))

(defun dump-viewport (key)
  (gl-get-integerv gl_viewport *dbg-viewport-r*)
  (format t "~&dump-viewport> ~a: ~a" key
    (loop for n from 0 to 3
        collecting (eltgli *dbg-viewport-r* n))))  

(defparameter *gl-ints-4*
  (fgn-alloc 'glint 4 :ignore))

(defun gl-get-ints-4 (gl-param)
  (gl-get-integerv gl-param *gl-ints-4*)
  (loop for n below 4
      collecting (eltgli *gl-ints-4* n)))

(defun ogl-raster-pos-get ()
  (gl-get-ints-4 gl_current_raster_position))

(defmacro with-bitmap-shifted ((x y) &body body)
  (let ((xy (gensym)))
  `(let ((,xy (cons ,x ,y)))
     (ogl-pen-move (car ,xy) (cdr ,xy))
     (prog1
         (progn ,@body)
       (ogl-pen-move (- (car ,xy)) (- (cdr ,xy)))))))

(defun ogl-pen-move (x y)
  ;;(ukt::trc "ogl-pen-moving" x y)
  (gl-bitmap 0 0 0 0 x y (cffi:null-pointer)))

(defclass ogl-texture ()
  ((texture-name :accessor texture-name :initform nil)
   (texture-precedence :accessor texture-precedence :initform 0)))

(defun flatten (&rest args)
  (mapcan (lambda (arg)
            (if (consp arg)
                (mapcan 'flatten arg)
              (list arg))) args))


(defparameter *dump-matrix* (fgn-alloc 'glfloat 16 :dump-matrix))
#+(or)
(defun dump-matrix (matrix-id msg)
  (gl-get-floatv matrix-id *dump-matrix*)
  (format t "~&~a > ~a matrix> ~{~a ~}" msg
    (cond ((eql matrix-id gl_modelview_matrix) 'modelview)
      ((eql matrix-id gl_projection_matrix) 'projection))
    (loop for n below 16 collecting (eltf *dump-matrix* n))))



